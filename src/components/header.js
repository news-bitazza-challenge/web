import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { NavLink } from 'react-router-dom'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'


const useStyles = makeStyles(theme => ({
	navLink: {
    color: 'inherit',
    textDecoration: 'none',
    display: 'block',
  },
}))

const Header = () => {
	const classes = useStyles()
  return (
		<AppBar position="static">
			<Toolbar>
				<NavLink exact className={classes.navLink} to={`/`}>
					<Typography variant="h6" >
						News
					</Typography>
				</NavLink>
			</Toolbar>
		</AppBar>
  )
}

export default Header
