import React from 'react'
import { orderBy } from 'lodash'
import { NavLink } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import PublicIcon from '@material-ui/icons/Public'
import Box from '@material-ui/core/Box'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'
import MenuItem from '@material-ui/core/MenuItem'
import TextField from '@material-ui/core/TextField'
import api from '../utils/web-api'


const useStyles = makeStyles(theme => ({
	card: {
		height: '100%'
	},
  media: {
    height: '140px',
	},
	publicIcon: {
		paddingRight: theme.spacing(1),
		color: theme.palette.text.secondary,
	},
	navLink: {
    color: 'inherit',
    textDecoration: 'none',
    display: 'block',
  },
}))

const Home = () => {
	const classes = useStyles()
	const [allNews, setAllNews] = React.useState([])
	const [sort, setSort] = React.useState('desc')

	const handleSortChange = (e) => {
		setAllNewsWithSorting(allNews, e.target.value)
		setSort(e.target.value)
	}

	const setAllNewsWithSorting = (data, sort) => {
		setAllNews(orderBy(data, ['publish'], [sort]))
	}

	React.useEffect(() => {
		api.fetchAllnews().then(res => {
			const allNews = res.data.map(news => ({...news, publish: new Date(news.publish)}))
			setAllNewsWithSorting(allNews, sort)
		})
	}, [])
	
  return (
		<Grid container >
			<Grid item xs={12}>
				<Box display="flex" justifyContent="flex-end">
					<TextField
						select
						label="Sort by published"
						value={sort}
						onChange={handleSortChange}
						margin="normal">
							<MenuItem value="desc">Descending to ascending</MenuItem>
							<MenuItem value="asc">Ascending to descending</MenuItem>
					</TextField>
				</Box>
			</Grid>
			<Grid item xs={12}>
				<Grid container spacing={2}>
					{allNews.map(news => (
					<Grid key={news.slug} item xs={12} sm={6} md={4}>
						<NavLink className={classes.navLink} to={`/news/${news.slug}`}>
							<Card className={classes.card}>
								<CardActionArea className={classes.cardActionArea}>
									<CardMedia
										className={classes.media}
										image={news.image}
										title="Contemplative Reptile"/>
									<CardContent>
										<Typography gutterBottom variant="h5" component="h2">
											{news.title}
										</Typography>
										<Typography variant="body2" color="textSecondary" component="p">
											{news.description.substring(0, 200)}...
										</Typography>
										<Box mt={2} display="flex" flexDirection="row" alignItems="center">
											<PublicIcon className={classes.publicIcon}/>
											<Typography variant="body2" color="textSecondary" component="p">
												{news.publish.toLocaleString()}
											</Typography>
										</Box>
									</CardContent>
								</CardActionArea>
							</Card>
						</NavLink>
					</Grid>
				))}
				</Grid>
			</Grid>
		</Grid>
	)
}


export default Home