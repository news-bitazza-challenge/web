import React from 'react'
import { withRouter } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import PublicIcon from '@material-ui/icons/Public'
import Box from '@material-ui/core/Box'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'
import api from '../utils/web-api'


const useStyles = makeStyles(theme => ({
  media: {
		height: '160px',
		[theme.breakpoints.up('sm')]: {
			height: '280px',
		},
		[theme.breakpoints.up('md')]: {
			height: '350px',
		},
		[theme.breakpoints.up('lg')]: {
			height: '500px',
		},
	},
	publicIcon: {
		paddingRight: theme.spacing(1),
		color: theme.palette.text.secondary,
	},
}))

const NewsPage = (props) => {
	const classes = useStyles()
	const [news, setNews] = React.useState(false)

	React.useEffect(() => {
		const { match } = props
		api.fetchNews(match.params.slug).then(res => setNews(res.data))
	}, [])
	
  return (
		<Grid container >
			{news && 
				<Grid key={news.slug} item >
					<Card>
						<CardActionArea className={classes.cardActionArea}>
							<CardMedia
								className={classes.media}
								image={news.image}
								title="Contemplative Reptile"/>
							<CardContent>
								<Typography gutterBottom variant="h4" component="h2">
									{news.title}
								</Typography>
								<Typography variant="body2" color="textSecondary" component="p">
									{news.description}
								</Typography>
								<Box mt={2} display="flex" flexDirection="row" alignItems="center">
									<PublicIcon className={classes.publicIcon}/>
									<Typography variant="body2" color="textSecondary" component="p">
										{news.publish.toLocaleString()}
									</Typography>
								</Box>
							</CardContent>
						</CardActionArea>
					</Card>
				</Grid>
			}
		</Grid>
	)
}

export default withRouter(NewsPage)
