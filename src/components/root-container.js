import React from 'react'
import { Route } from 'react-router-dom'
import Container from '@material-ui/core/Container'
import { makeStyles } from '@material-ui/core/styles'
import Header from './header'
import HomePage from './home-page'
import NewsPage from './news-page'


const useStyles = makeStyles(theme => ({
  rootContainer: {
    padding: 0,
  },
  content: {
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(7),
    [theme.breakpoints.up('lg')]: {
      marginTop: theme.spacing(15),
      marginBottom: theme.spacing(10),
    }
  },
}))

const RootContainer = () => {
  const classes = useStyles()
  return (
    <Container className={classes.rootContainer} maxWidth={false}>
      <Header />
      <Container className={classes.content}  maxWidth='lg'>
        <Route exact path='/' component={HomePage} />
        <Route path='/news/:slug' component={NewsPage} />
      </Container>
    </Container>
  )
}

export default RootContainer
