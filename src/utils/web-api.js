import axios from 'axios'


const webAPI = () => {
  const server = axios.create()
  server.interceptors.response.use(
    response => ({ ...response, err: false }),
    error => ({ ...error.response, err: true }),
  )

  return {
    fetchAllnews: async () => {
      const res = await server.get('/api/news/public/news')
      return res
    },
    fetchNews: async (slug) => {
      const res = await server.get(`/api/news/public/news/${slug}`)
      return res
    },
  }
}

export default webAPI()
