import React from 'react'
import { MuiThemeProvider } from '@material-ui/core/styles'
import { BrowserRouter as Router } from 'react-router-dom'
import theme from './theme'
import RootContainer from './components/root-container'


const App = () => {
  return (
    <MuiThemeProvider theme={theme}>
      <Router>
        <RootContainer />
      </Router>
    </MuiThemeProvider>
  )
}

export default App
